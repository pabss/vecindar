//
//  MyNavViewController.swift
//  WashMe
//
//  Created by Pablo A. Rivera on 9/26/16.
//  Copyright © 2016 Pixnabi. All rights reserved.
//

import UIKit

class MyNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Config navigation controller ui
//        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = false
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor(netHex: 0x006600)
    }
}
