//
//  ProjectViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/16/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import Parse
import MapKit

class ProjectViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var albumCollection: UICollectionView!
    @IBOutlet weak var budgetTable: UITableView!
    @IBOutlet weak var contactEmail: UILabel!
    @IBOutlet weak var contactPhone: UILabel!
    @IBOutlet weak var budgetTotalLabel: UILabel!
    
    var collectionHeight: CGFloat!
    var project: PFObject!
    var album: [PFFile]!
    var budgetList = [Dictionary<String,Any>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.collectionHeight = self.albumCollection.frame.height
        self.albumCollection.reloadData()
    }
    
    @IBAction func donateToProject(_ sender: AnyObject) {
        
    }
}

// MARK: Class custom functions
extension ProjectViewController {
    
    func updateUI() {
        
        self.donateBtn.layer.cornerRadius = 10
        
        self.budgetTable.estimatedRowHeight = 55
        self.budgetTable.rowHeight = UITableViewAutomaticDimension
        self.budgetTable.layer.borderWidth = 1
        self.budgetTable.layer.borderColor = UIColor(netHex: 0x009933).cgColor
        self.budgetTable.layer.cornerRadius = 10
        
        if self.project != nil {
            if let title = self.project["title"] as? String {
                self.titleLabel.text = title
            }
            
            if let album = self.project["album"] as? [PFFile] {
                self.album = album
                self.albumCollection.reloadData()
            }
            if let budget = self.project["budgetBreakdown"] as? [Dictionary<String,Any>] {
                self.budgetList = budget
                let total = self.getBudgetTotal(budgetList: self.budgetList)
                self.budgetTotalLabel.text = "Total: $\(total)"
                self.budgetTable.reloadData()

            }
            
            if let desc = self.project["description"] as? String {
                self.descLabel.text = desc
            }
            
            if let contactEmail = self.project["contactEmail"] as? String {
                self.contactEmail.text = contactEmail
            }
            
            if let contactPhone = self.project["contactPhone"] as? String {
                self.contactPhone.text = contactPhone
            }
            
            if let location = self.project["location"] as? PFGeoPoint {
                self.centerMapOnLocation(location: location)
            }
        }
    }
    
    func getBudgetTotal(budgetList: [Dictionary<String,Any>]) -> Double {
        var total = 0.0
        for budget in budgetList {
            if let priceStr = budget["price"] as? String,
                let price = Double(priceStr) {
                total += price
            }
        }
        return total
    }
    
    func centerMapOnLocation(location: PFGeoPoint) {
        let cllocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(cllocation.coordinate,
                                                                  500 * 2.0, 500 * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        let annotation = MKPointAnnotation()
        let location = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        annotation.coordinate = location
        mapView.addAnnotation(annotation)
    }
}

// MARK: Album Collection View Data Source and Delegate
extension ProjectViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.album != nil ? self.album.count : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AlbumCellProyect
        let data = self.album[indexPath.row]
        cell.loadCell(data: data, index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionHeight - 10, height: self.collectionHeight - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

extension ProjectViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.budgetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BudgetCellProject
        let data = self.budgetList[indexPath.row]
        cell.loadCell(data: data, index: indexPath.row)
        return cell
    }

}

class AlbumCellProyect: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    func loadCell(data: PFFile, index: Int) {
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        imgView.isHidden = false
        imgView.image = nil
        if let urlStr = data.url,
            let url = URL(string: urlStr) {
            self.imgView.hnk_setImage(from: url)
        }
    }
}
class BudgetCellProject: UITableViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var checkImg: UIImageView!
    
    func loadCell(data: Dictionary<String,Any>, index: Int) {
        self.selectionStyle = .none
        if let desc = data["desc"] as? String {
            self.descLabel.text = desc
        }
        
        if let price = data["price"] as? String {
            self.priceLabel.text = price
        }
        
        if let done = data["done"] as? Bool {
            self.checkImg.isHidden = true
            if done {
                self.checkImg.isHidden = false
            }
        }
        
    }
}
