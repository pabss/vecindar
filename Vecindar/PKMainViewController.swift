//
//  PKMainViewController.swift
//  WashMe
//
//  Created by Pablo A. Rivera on 9/13/16.
//  Copyright © 2016 Pixnabi. All rights reserved.
//

import UIKit
import PKRevealController

class PKMainViewController: PKRevealController, PKRevealing {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let front = storyboard?.instantiateViewController(withIdentifier: "homeView")
        let right = storyboard?.instantiateViewController(withIdentifier: "leftView")
        self.frontViewController = front
        self.leftViewController = right
    }
}
