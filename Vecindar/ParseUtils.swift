//
//  ParseUtils.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/15/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import Parse

class ParseUtils: NSObject {
    
    class func userIsLoggedin() -> Bool {
        if PFUser.current() != nil {
            return true
        }
        return false
    }
    
}
