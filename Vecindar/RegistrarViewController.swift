//
//  RegistrarViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/15/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import Parse
import MBProgressHUD

class RegistrarViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var loginFormView: UIView!
    // Register form fields
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var signupActionBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    // Login form fields
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    var isLogin = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loginFormView.isHidden = true
        self.logo.layer.cornerRadius = 10
        self.registerBtn.layer.cornerRadius = 10
    }

    @IBAction func registerUser(_ sender: AnyObject) {
        if isLogin {
            self.loginUser()
        } else {
            self.registerUser()
        }
    }
    
    @IBAction func toggleForm(sender: UIButton) {
        isLogin = !isLogin
        if isLogin {
            loginFormView.isHidden = false
//            self.title = "Iniciar"
            signupActionBtn.setTitle("Iniciar Sesión", for: .normal)
            
            // Toggle the senders title
            sender.setTitle("Registrarme", for: .normal)
        } else {
            loginFormView.isHidden = true
//            self.title = "Registro"
            signupActionBtn.setTitle("Registrarme", for: .normal)
            
            // Toggle the senders title
            sender.setTitle("Iniciar Sesión", for: .normal)
        }
    }
}

extension RegistrarViewController {
    func registerUser() {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        if let email = emailField.text,
            let pass = passField.text,
            let name = nameField.text {
            let user = PFUser()
            user.email = email
            user.username = email
            user.password = pass
            user["name"] = name
            user.signUpInBackground(block: { (success, error) in
                indicator.hide(animated: true)
                if error != nil {
                    print("Error register: \(error)")
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            })
        } else {
            Utils.showSimpleAlertWithTitle(title: "", message: "Favor de llenar campos requeridos", viewController: self)
        }
    }
    
    
    func loginUser() {
        print("logging in")
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        if let username = self.usernameField.text,
            let pass = self.passwordField.text {
            PFUser.logInWithUsername(inBackground: username, password: pass, block: { (user, error) in
                indicator.hide(animated: true)
                if error != nil {
                    Utils.showSimpleAlertWithTitle(title: "", message: "Correo electrónico o contraseña incorrecta", viewController: self)
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            })
        }
    }
}
