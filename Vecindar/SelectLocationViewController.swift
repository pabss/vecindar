//
//  SelectLocationViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/15/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import MapKit
import Parse

class SelectLocationViewController: UIViewController, CLLocationManagerDelegate, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    
    var selectedLocation: PFGeoPoint!
    var solicitudView: NuevaSolicitudViewController!
    var locationManager: CLLocationManager! {
        didSet {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        self.locationManager = CLLocationManager()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissSearch))
        tapGesture.numberOfTapsRequired = 1
        self.mapView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("updating location")
        let annotation = MKPointAnnotation()
        let location = CLLocationCoordinate2D(latitude: locations[0].coordinate.latitude, longitude: locations[0].coordinate.longitude)
        annotation.coordinate = location
        mapView.addAnnotation(annotation)
        centerMapOnLocation(location: location)
        locationManager.stopUpdatingLocation()
    }
    
    
    func centerMapOnLocation(location: CLLocationCoordinate2D) {
        let cllocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        self.selectedLocation = PFGeoPoint(latitude: location.latitude, longitude: location.longitude)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(cllocation.coordinate,
                                                                  500 * 2.0, 500 * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func dismissSearch() {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    @IBAction func addLocation(_ sender: AnyObject) {
        if self.selectedLocation != nil {
            self.solicitudView.locationSelected(location: self.selectedLocation)
        }
        self.dismiss(animated: true, completion: nil)
    }
}
