//
//  NuevaSolicitudViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/15/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import Parse
import DZNEmptyDataSet
import Haneke
import MBProgressHUD

class NuevaSolicitudViewController: UIViewController {
    
    // Project fields
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var telephoneField: UITextField!
    
    @IBOutlet weak var deleteImageLabel: UILabel!
    @IBOutlet weak var budgetTable: UITableView!
    @IBOutlet weak var budgetTotal: UILabel!
    @IBOutlet weak var budgetDescField: UITextField!
    @IBOutlet weak var budgetPriceField: UITextField!
    @IBOutlet weak var albumCollection: UICollectionView!
    @IBOutlet weak var addLocationBtn: UIButton!
    @IBOutlet weak var addBudgetBtn: UIButton!
    @IBOutlet weak var addProyectBtn: UIButton!
    
    var budgetList = [Dictionary<String,Any>]()
    var album = [UIImage]()
    var selectedLocation: PFGeoPoint!
    
    var viewWidth: CGFloat! = 0
    var collectionHeight: CGFloat! = 0
    var imagePicker: UIImagePickerController! {
        didSet {
            imagePicker.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.deleteImageLabel.isHidden = true
        self.imagePicker = UIImagePickerController()
        
        self.updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewWidth = self.view.frame.width
        self.collectionHeight = self.albumCollection.frame.height
        self.albumCollection.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectLocationSegue" {
            let locationView = segue.destination as! SelectLocationViewController
            locationView.solicitudView = self
        }
    }
    
    @IBAction func addBudgetToList(_ sender: AnyObject) {
        if let desc = self.budgetDescField.text,
            let price = self.budgetPriceField.text {
            if !desc.isEmpty && !price.isEmpty {
                let budget = ["desc":desc, "price":price, "done":false] as [String : Any]
                self.budgetList.append(budget)
                let total = self.getBudgetTotal(budgetList: self.budgetList)
                self.budgetTotal.text = "Total: $\(total)"
                self.budgetPriceField.text = nil
                self.budgetDescField.text = nil
                self.budgetPriceField.resignFirstResponder()
                self.budgetDescField.resignFirstResponder()
                self.budgetTable.reloadData()
            } else {
                Utils.showSimpleAlertWithTitle(title: "", message: "Favor de llenar campos requeridos.", viewController: self)
            }
        }
    }
    
    @IBAction func deleteBudget(_ sender: AnyObject) {
        self.budgetList.remove(at: sender.tag)
        self.budgetTable.reloadData()
    }
    
    @IBAction func addPicToAlbum(_ sender: AnyObject) {
        Utils.showImagepicker(viewController: self, imagePicker: self.imagePicker, title: "Añadir al Album", message: "¿Desea tomar una foto nueva o escoger de la librería?")
    }
    
    @IBAction func createProject(_ sender: AnyObject) {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        let total = self.getBudgetTotal(budgetList: self.budgetList)
        let files = self.getAlbumFiles(album: self.album)
        let project = PFObject(className: "Solicitudes")
        project["title"] = self.titleField.text!
        project["location"] = self.selectedLocation
        project["description"] = self.descTextView.text!
        project["budget"] = total
        project["album"] = files
        project["budgetBreakdown"] = self.budgetList
        project["contactEmail"] = self.emailField.text!
        project["contactPhone"] = self.telephoneField.text!
        project.saveInBackground { (success, error) in
            indicator.hide(animated: true)
            if error != nil {
                print("error saving project: \(error)")
            } else {
                print("project saved!")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    
}

// MARK: Budget List TableViewDataSource/Delegate
extension NuevaSolicitudViewController: UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.budgetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BudgetCell
        let data = self.budgetList[indexPath.row]
        cell.loadCell(data: data, index: indexPath.row)
        return cell
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let message = "Añadir presupuestos"
        let attrs = [NSFontAttributeName : UIFont.systemFont(ofSize: 13.0), NSForegroundColorAttributeName : UIColor.lightGray]
        let atributedStr = NSAttributedString(string: message, attributes: attrs)
        return atributedStr
    }
    
}

// MARK: Collection view photo albums
extension NuevaSolicitudViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.album.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! AlbumCell
        cell.loadCell(data: nil, index: indexPath.row)
        if indexPath.row != 0 {
            let data = self.album[indexPath.row - 1]
            cell.loadCell(data: data, index: indexPath.row)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Eliminar", message: "¿Desea elminar la imagen?", preferredStyle: .alert)
        let delete = UIAlertAction(title: "Eliminar", style: .destructive) { (delete) in
            self.album.remove(at: indexPath.row - 1)
            if self.album.count == 0 {
                self.deleteImageLabel.isHidden = true
            }
            self.albumCollection.reloadData()
        }
        let action = UIAlertAction(title: "Cancelar", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(delete)
        self.present(alert, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionHeight - 10, height: self.collectionHeight - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}

// MARK: Image Picker delegate and custom class functions
extension NuevaSolicitudViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.album.append(pickedImage)
            self.deleteImageLabel.isHidden = false
            self.albumCollection.reloadData()
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    // update ui objects. corner radius, borders width and colors
    func updateUI() {
        self.revealController.disablesFrontViewInteraction = true
        
        self.budgetTable.estimatedRowHeight = 55
        self.budgetTable.rowHeight = UITableViewAutomaticDimension
        
        self.addLocationBtn.layer.cornerRadius = 10
        self.addBudgetBtn.layer.cornerRadius = 10
        self.addProyectBtn.layer.cornerRadius = 10
        
        self.descTextView.layer.borderWidth = 1
        self.descTextView.layer.borderColor = UIColor(netHex: 0x009933).cgColor
        self.descTextView.layer.cornerRadius = 10
        
        self.budgetTable.layer.borderWidth = 1
        self.budgetTable.layer.borderColor = UIColor(netHex: 0x009933).cgColor
        self.budgetTable.layer.cornerRadius = 10
    }
    
    func getAlbumFiles(album: [UIImage]) -> [PFFile] {
        var files = [PFFile]()
        for photo in album {
            if let data = UIImageJPEGRepresentation(photo, 0.1) {
                if let file = PFFile(data: data) {
                    files.append(file)
                }
            }
        }
        return files
    }
    
    func getBudgetTotal(budgetList: [Dictionary<String,Any>]) -> Double {
        var total = 0.0
        for budget in budgetList {
            if let priceStr = budget["price"] as? String,
                let price = Double(priceStr) {
                total += price
            }
        }
        return total
    }
    
    // Custom function to execute when map pup up "Añadir" button pressed
    func locationSelected(location: PFGeoPoint) {
        self.selectedLocation = location
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: location.latitude, longitude: location.longitude)
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
            indicator.hide(animated: true)
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                if let addressInfo = placemark.addressDictionary {
                    var address = ""
                    // Street address
                    if let street = addressInfo["Thoroughfare"] as? NSString
                    {
                        print("street: \(street)")
                        address = "\(street)"
                    }
                    
                    // City
                    if let city = addressInfo["City"] as? NSString
                    {
                        print("city: \(city)")
                        address = "\(address) \(city)"
                    }
                    
                    // Zip code
                    if let zip = addressInfo["ZIP"] as? NSString
                    {
                        print("zip: \(zip)")
                        address = "\(address) \(zip)"
                    }
                    print("Address: \(address)")
                    self.addLocationBtn.setTitle(address, for: .normal)
                }
            }
        }
    }
}

class BudgetCell: UITableViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    func loadCell(data: Dictionary<String,Any>, index: Int) {
        deleteBtn.tag = index
        self.selectionStyle = .none
        if let desc = data["desc"] as? String {
            self.descLabel.text = desc
        }
        
        if let price = data["price"] as? String {
            self.priceLabel.text = price
        }
    }
}

class AlbumCell: UICollectionViewCell {
    
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    func loadCell(data: UIImage!, index: Int) {
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        addBtn.isHidden = true
        imgView.isHidden = true
        if index == 0 {
            addBtn.isHidden = false
        } else {
            imgView.isHidden = false
            imgView.image = data
        }
    }
}
