//
//  SolicitudesViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/15/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import MapKit
import Parse
import MBProgressHUD

class SolicitudesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var solicitudes: [PFObject]!
    var index = 0
    
    var menuBtn: UIBarButtonItem!
    var locationManager: CLLocationManager! {
        didSet {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.revealController.setMinimumWidth(self.view.frame.width / 1.4, maximumWidth: self.view.frame.width / 1.4, for: self.revealController.leftViewController)
        
        menuBtn = UIBarButtonItem(image: UIImage(named:"menu2"), style: .plain, target: self, action: #selector(self.toggleSideMenu))
        self.navigationItem.leftBarButtonItem = menuBtn
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.getSolicitudes()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "projectDescSegue" {
            let destination = segue.destination as! ProjectViewController
            destination.project = self.solicitudes[index]
        }
    }

}

// MARK: Class custom functions
extension SolicitudesViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    func toggleSideMenu() {
        if !(self.revealController.isPresentationModeActive)
        {
            self.revealController.enterPresentationMode(animated: true, completion: nil)
        }
        else
        {
            self.revealController.resignPresentationModeEntirely(true, animated: true, completion: nil)
        }
    }
    
    func getSolicitudes() {
        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
        let query = PFQuery(className: "Solicitudes")
        query.findObjectsInBackground { (objects, error) in
            indicator.hide(animated: true)
            if error != nil {
                print("error retreiving data: \(error)")
            } else {
                if let solicitudes = objects {
                    self.solicitudes = solicitudes
                    self.tableView.reloadData()
                }
            }
        }
    }
}

// MARK: Solicitudes table view delegate and data source
extension SolicitudesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.solicitudes != nil ? self.solicitudes.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SolicitudCell
        let data = self.solicitudes[indexPath.row]
        cell.loadCell(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.index = indexPath.row
        self.performSegue(withIdentifier: "projectDescSegue", sender: self)
    }
}

class SolicitudCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var projectImg: UIImageView!
    
    func loadCell(data: PFObject) {
        self.selectionStyle = .none
        self.projectImg.layer.cornerRadius = 10
        if let title = data["title"] as? String {
            self.titleLabel.text = title
        }
        
        if let desc = data["description"] as? String {
            self.descLabel.text = desc
        }
        
        if let album = data["album"] as? [PFFile] {
            if album.count > 0 {
                let imgFile = album[0]
                if let urlStr = imgFile.url,
                    let url = URL(string: urlStr) {
                    self.projectImg.hnk_setImage(from: url)
                }
            }
        }
    }
    
}
