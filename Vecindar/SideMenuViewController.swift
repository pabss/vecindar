//
//  SideMenuViewController.swift
//  WashMe
//
//  Created by Pablo A. Rivera on 9/13/16.
//  Copyright © 2016 Pixnabi. All rights reserved.
//

import UIKit
import PKRevealController
import Parse
import DZNEmptyDataSet

class SideMenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let userOptions = [["title":"Proyectos", "icon": UIImage(named: "list.png")!],
                       ["title":"Proyecto Nuevo", "icon": UIImage(named: "add.png")!],
                       ["title":"Mis Proyectos","icon":UIImage(named: "my_projects.png")!],
                       ["title":"Proyectos Interesados", "icon":UIImage(named: "favs.png")!],
                       ["title":"Perfil", "icon": UIImage(named: "profile.png")!],
                       ["title":"Cerrar Sesión", "icon":UIImage(named: "logout.png")!]]
    var tableData: [Dictionary<String,Any>]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableData = ParseUtils.userIsLoggedin() ? userOptions : nil
        tableView.reloadData()
    }
    
}

// Mark: Class custom functions
extension SideMenuViewController {
    
}

// MARK: Table View Delegate and Data Source for the side menu
extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData != nil ? self.tableData.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideCell
        cell.selectionStyle = .none
        let opt = self.tableData[indexPath.row]
        cell.loadCell(opt: opt)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
//            crearSolicitudView
            if let crearSolicitudView = self.storyboard?.instantiateViewController(withIdentifier: "crearSolicitudView") {
                self.revealController.resignPresentationModeEntirely(true, animated: true, completion: nil)
                if let frontView = self.revealController.frontViewController as? UINavigationController {
                    frontView.pushViewController(crearSolicitudView, animated: true)
                }
            }
        }
        if indexPath.row == 5 {
            self.revealController.resignPresentationModeEntirely(true, animated: true, completion: nil)
            PFUser.logOutInBackground(block: { (error) in
                if error == nil {
                    print("user logged out")
                }
            })
        }
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let message = "¡Registrese!"
        let attrs = [NSFontAttributeName : UIFont.systemFont(ofSize: 19.0), NSForegroundColorAttributeName : UIColor.darkGray]
        let atributedStr = NSAttributedString(string: message, attributes: attrs)
        return atributedStr
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "Al registrarse puede públicar proyectos de comuidad."
        
        var para = NSMutableParagraphStyle()
        //        para.lineBreakMode = NSLineBreakMode.byWordWrapping
        para.alignment = NSTextAlignment.center
        
        let attribs = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 14),
            NSForegroundColorAttributeName: UIColor.lightGray,
            NSParagraphStyleAttributeName: para
        ]
        
        return NSAttributedString(string: text, attributes: attribs)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let text = "Registrarme/Iniciar Sesión"
        let attribs = [
            NSFontAttributeName: UIFont.systemFont(ofSize: 16),
            NSForegroundColorAttributeName: view.tintColor
        ] as [String : Any]
        
        return NSAttributedString(string: text, attributes: attribs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        if let register = self.storyboard?.instantiateViewController(withIdentifier: "registerView") {
            self.revealController.resignPresentationModeEntirely(true, animated: true, completion: nil)
            if let frontView = self.revealController.frontViewController as? UINavigationController {
                
                frontView.pushViewController(register, animated: true)
            }
        }
    }

}

class SideCell: UITableViewCell {
    
    @IBOutlet weak var titleOpt: UILabel!
    @IBOutlet weak var iconOpt: UIImageView!
    
    func loadCell(opt: Dictionary<String,Any>) {
        if let title = opt["title"] as? String {
            self.titleOpt.text = title
        }
        if let icon = opt["icon"] as? UIImage {
            self.iconOpt.image = icon
        }
    }
}
