//
//  DonateViewController.swift
//  Vecindar
//
//  Created by Pablo A. Rivera on 10/16/16.
//  Copyright © 2016 Pablo A. Rivera. All rights reserved.
//

import UIKit
import MBProgressHUD

class DonateViewController: UIViewController {

    @IBOutlet weak var donateView: UIView!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var thankyouLabel: UILabel!
    
    var json: [String:AnyObject]!
    
    var sessionTask: URLSessionDataTask!
    var paymentTask: URLSessionDataTask!
    var statusTask: URLSessionDataTask!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.thankyouLabel.isHidden = true
        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        self.donateBtn.layer.cornerRadius = 10
        
        self.donateView.layer.borderColor = UIColor(netHex: 0x009933).cgColor
        self.donateView.layer.borderWidth = 1
        self.donateView.layer.cornerRadius = 10
        self.getSession()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.sessionTask != nil {
            self.sessionTask.cancel()
        }
        
        if self.statusTask != nil {
            self.statusTask.cancel()
        }
    }

    @IBAction func addDonation(_ sender: AnyObject) {
        self.makePayment(token: self.json["token"] as! String)
    }

    @IBAction func cancelDonation(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}

// ATH Movile API
extension DonateViewController {
    func getSession() {
//        let indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
//        indicator.label.text = "Procesando"
        
        let url = URL(string: "http://athmapi.westus.cloudapp.azure.com/athm/requestSession")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = "{\n  \"commUsername\": \"pabsdev\",\n  \"commPassword\": \"12345\"\n}".data(using: .utf8)
        
        self.sessionTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response, let data = data {
                print(response)
                if let jsonStr = String(data: data, encoding: .utf8) {
                    if let json = self.convertStringToDictionary(text: jsonStr) {
                        print("JSON: \(json)")
                        self.json = json
                    }
                }
            } else {
                print(error)
            }
        }
        self.sessionTask.resume()
    }
    
    func makePayment(token: String) {
        if self.sessionTask != nil {
            self.sessionTask.cancel()
            self.sessionTask = nil
        }
        if let phone = self.phoneField.text,
            let amount = self.amountField.text {
            let url = URL(string: "http://athmapi.westus.cloudapp.azure.com/athm/requestPayment")!
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = "{\n  \"token\": \"\(token)\",\n  \"phone\": \"\(phone)\",\n  \"amount\": \(amount)\n}".data(using: .utf8)
            
            self.paymentTask = URLSession.shared.dataTask(with: request) { data, response, error in
                if let response = response, let data = data {
                    print(response)
                    if let jsonStr = String(data: data, encoding: .utf8) {
                        if let json = self.convertStringToDictionary(text: jsonStr) {
                            print("JSON: \(json)")
                            self.getStatus(reference: json["referenceNumber"] as! String, token: self.json["token"] as! String)
                        }
                    }
                } else {
                    print(error)
                }
            }
            
            self.paymentTask.resume()
        }
    }
    
    func getStatus(reference: String, token: String) {
        if self.paymentTask != nil {
            self.paymentTask.cancel()
        }
        let url = URL(string: "http://athmapi.westus.cloudapp.azure.com/athm/verifyPaymentStatus")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = "{\n  \"token\": \"\(token)\",\n  \"referenceNumber\": \"\(reference)\"\n}".data(using: .utf8)
        
        self.statusTask = URLSession.shared.dataTask(with: request) { data, response, error in
            if let response = response, let data = data {
                print(response)
                DispatchQueue.main.async {
                    if let jsonStr = String(data: data, encoding: .utf8) {
                        if let json = self.convertStringToDictionary(text: jsonStr) {
                            print("JSON: \(json)")
                            if let status = json["status"] as? String {
                                print("Status: \(status)")
                                if status == "COMPLETED" {
                                    self.donateBtn.setTitle("Contribución Completada", for: .normal)
                                    self.donateBtn.isEnabled = false
                                    self.thankyouLabel.isHidden = false
                                }
                            }
                        }
                    }
                }
                
            } else {
                print(error)
            }
        }
        
        self.statusTask.resume()
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}
