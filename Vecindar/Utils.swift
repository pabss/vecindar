//
//  Utils.swift
//  Vendy
//
//  Created by Pablo A. Rivera on 10/2/15.
//  Copyright © 2015 Pablo A. Rivera. All rights reserved.
//

import UIKit

class Utils: NSObject {
    
    // MARK: Show Simple Alert Message
    class func showSimpleAlertWithTitle(title: String!, message: String, viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    // MARK: ImagePicker Utils
    class func showImagepicker(viewController: UIViewController, imagePicker: UIImagePickerController, title: String?, message: String?){
        let alert = UIAlertController(title: title != nil ? title : "", message: message != nil ? message : "Chose from Library or New Picture", preferredStyle:   UIAlertControllerStyle.alert)
        
        let select = UIAlertAction(title: "Librería", style: UIAlertActionStyle.default) { (library) -> Void in
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            viewController.present(imagePicker, animated: true, completion: nil)
        }
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            let newPic = UIAlertAction(title: "Foto nueva", style: UIAlertActionStyle.default) { (library) -> Void in
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                viewController.present(imagePicker, animated: true, completion: nil)
                
            }
            alert.addAction(newPic)
        }
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(select)
        alert.addAction(cancel)
        viewController.present(alert, animated: true, completion: nil)
    }
}
